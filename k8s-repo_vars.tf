variable "k8s_repo_name" { default = "k8s-repo" }
variable "k8s_trigger_branch_name" { default = "master" }
variable "cloudbuild_filename" { default = "cloudbuild.yaml" }