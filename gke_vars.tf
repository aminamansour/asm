# gke clusters

    variable "kubernetes_version" { default = "1.14" }
    variable "gke_asm_r1" { default = "gke-asm-1-r1-prod" }
    variable "gke_asm_r2" { default = "gke-asm-2-r2-prod" }

    variable "gke_dev1-r1a" { default = "gke-1-apps-r1a-prod" }
    variable "gke_dev1-r1b" { default = "gke-2-apps-r1b-prod" }

    variable "gke_dev2-r2a" { default = "gke-3-apps-r2a-prod" }
    variable "gke_dev2-r2b" { default = "gke-4-apps-r2b-prod" }

    variable "kubemesh" { default = "kubemesh" }